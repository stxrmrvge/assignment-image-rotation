#ifndef ROTATE_IMAGE_IMAGE_H
#define ROTATE_IMAGE_IMAGE_H

#include <inttypes.h>
#include <malloc.h>

struct image 
{
	uint32_t width, height;
	struct pixel* data;
};

struct pixel
{
	uint8_t b;
	uint8_t g;
	uint8_t r;
};

struct image create_image(uint32_t width, uint32_t height);
struct image rotate_image(struct image const source);

#endif