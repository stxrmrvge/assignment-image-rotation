#ifndef ROTATE_IMAGE_BMP_FORMAT_H
#define ROTATE_IMAGE_BMP_FORMAT_H

#include <stdio.h>
#include <inttypes.h>
#include <stdbool.h>
#include "status.h"
#include "image.h"

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

struct bmp_image
{
    struct bmp_header* header;
    struct pixel* data;
};

uint32_t get_padding(const size_t width);

enum read_status from_bmp(FILE* in, struct image* img);
enum write_status to_bmp(FILE* out, struct image const* img);

// BMP Header
bool read_header(FILE* f, struct bmp_header* header);
bool read_header_from_file(const char* filename, struct bmp_header* header);

#endif