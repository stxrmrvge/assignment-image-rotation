#include "status.h"

static const char* const read_status_m[] = {
    [READ_OK] = "Your input file was successfully read.",
    [READ_CLOSE_ERROR] = "Error closing file.",
    [READ_PATH_ERROR] = "Read-path not exist.",
    [READ_INVALID_HEADER] = "Wrong header format.",
    [READ_OPEN_FILE_ERROR] = "Open file error.",
    [READ_ERROR] = "Read error.",
};

static const char* const write_status_m[] = {
    [WRITE_OK] = "Output file was updated.",
    [WRITE_CLOSE_ERROR] = "Error closing file.",
    [WRITE_ERROR] = "The process of writing wasn't finished. Output file with corrupted data.",
    [WRITE_INVALID_HEADER] = "Write error: invalid header.",
    [WRITE_PATH_ERROR] = "Write path error. Maybe you haven't access to create a new file."
};

const char* write_status_map(enum write_status status)
{
	return (char*)write_status_m[status];
}

const char* read_status_map(enum read_status status)
{
	return (char*)read_status_m[status];
}