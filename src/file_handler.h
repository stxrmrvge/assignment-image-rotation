#ifndef ROTATE_IMAGE_FILE_HANDLER_H
#define ROTATE_IMAGE_FILE_HANDLER_H

#include <stdio.h>
#include "status.h"

enum read_status file_read(FILE** file, const char* path);
enum write_status file_write(FILE** file, const char* path);

enum read_status file_close(FILE** file);

#endif