#include "image.h"

struct image create_image(uint32_t width, uint32_t height)
{
	struct image img = { 0, 0, 0 };
	img.width = width;
	img.height = height;
	img.data = malloc(width * height * sizeof(struct pixel));

	return img;
}


struct image rotate_image(struct image const source)
{
	uint32_t width = source.width;
	uint32_t height = source.height;
	struct image rotated_image = create_image(width, height);

	for (size_t i = 0; i < (size_t)height; i = i + 1)
	{
		for (size_t j = 0; j < width; j = j + 1)
		{
			*(rotated_image.data + i * width + j) = *(source.data + j * height + i);
		}
	}

	return rotated_image;
}