#include "bmp_format.h"
#include "image.h"

#define BM 19778
#define BIT_COUNT 24    // amount of bits per pixel and const because program supports only bmp 24bit
#define BMP_RESERVED_ZERO 0

uint32_t get_padding(const size_t width)
{
    size_t bytes = width * sizeof(struct pixel);
    if (bytes % 4) return 4 - (bytes % 4);
    else return 0;
}

bool read_header(FILE* f, struct bmp_header* header) {
    return fread(header, sizeof(struct bmp_header), 1, f);
}

bool read_header_from_file(const char* filename, struct bmp_header* header) {
    if (!filename) return false;
    FILE* f = fopen(filename, "rb");
    if (!f) return false;
    if (read_header(f, header)) {
        fclose(f);
        return true;
    }

    fclose(f);
    return false;
}

enum read_status from_bmp(FILE* in, struct image* img)
{
    struct bmp_header header;
    bool read_header_status = read_header(in, &header);
    if (!read_header_status) return READ_INVALID_HEADER;

    const uint32_t width = header.biWidth;
    const uint32_t height = header.biHeight;

    *img = create_image(width, height);
    const uint32_t padding = get_padding(width);

    for (size_t i = 0; i < height; i = i + 1)
    {
        size_t read_size = fread(img->data + (i * width), sizeof(struct pixel), width, in);
        if (read_size != width) return READ_ERROR;
        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;
}


enum write_status to_bmp(FILE* out, struct image const* img)
{
    const uint32_t width = img->width;
    const uint32_t height = img->height;

    struct bmp_header header;

    header.bfType = BM;
    header.bfileSize = width * height * sizeof(struct pixel);
    header.bfReserved = BMP_RESERVED_ZERO;
    header.bOffBits = BMP_RESERVED_ZERO;
    header.biSize = 40;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = 1;
    header.biBitCount = BIT_COUNT;
    header.biCompression = BMP_RESERVED_ZERO;
    header.biSizeImage = height * (width * sizeof(struct pixel) + width % 4);
    header.biXPelsPerMeter = BMP_RESERVED_ZERO;
    header.biYPelsPerMeter = BMP_RESERVED_ZERO;
    header.biClrUsed = BMP_RESERVED_ZERO;
    header.biClrImportant = BMP_RESERVED_ZERO;

    const uint32_t padding = get_padding(width);
    struct pixel empty_pixel = { 0, 0, 0 };         // need to write empty pixel if width not a multiply of 4

    size_t header_write_size = fwrite(&header, sizeof(struct bmp_header), 1, out);
    if (header_write_size != 1) return WRITE_ERROR;

    for (size_t i = 0; i < height; i = i + 1)
    {
        size_t write_size = fwrite(img->data + (i * width), sizeof(struct pixel), width, out);
        if (write_size != width) return WRITE_ERROR;

        size_t empty_pixel_size = fwrite(&empty_pixel, 1, padding, out);
        if (empty_pixel_size != 1) return WRITE_ERROR;
    }

    return WRITE_OK;
}