#ifndef ROTATE_IMAGE_STATUS_H
#define ROTATE_IMAGE_STATUS_H

enum read_status {
    READ_OK = 0,
    READ_PATH_ERROR,
    READ_CLOSE_ERROR,
    READ_INVALID_HEADER,
    READ_OPEN_FILE_ERROR,
    READ_CLOSE_OK,
    READ_ERROR,
};


enum  write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_FILE_DOESNT_EXIST,
    WRITE_CLOSE_ERROR,
    WRITE_INVALID_HEADER,
    WRITE_PATH_ERROR,
};

const char* write_status_map(enum write_status status);
const char* read_status_map(enum read_status status);

#endif