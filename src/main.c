#include "file_handler.h"
#include "bmp_format.h"
#include "image.h"
#include "utils.h"
#include "status.h"

// #include <vld.h>

void check_read_status(enum read_status status)
{
	if (status != READ_OK) err(read_status_map(status));
}

void check_write_status(enum write_status status)
{
	if (status != WRITE_OK) err(write_status_map(status));
}

int main(int argc, char** argv)
{
	if (argc != 3)
		printf("./rotate-image: path-to-source.bmp path-to-output.bmp");
	if (argc < 3) err("\nNot enough arguments\n");
	if (argc > 3) err("\nToo many arguments\n");

	// Open files to read and write
	FILE* file;
	enum read_status r_status = file_read(&file, argv[1]);
	check_read_status(r_status);

	FILE* file2;
	enum write_status w_status = file_write(&file2, argv[2]);
	check_write_status(w_status);

	// Read from source.bmp file to struct image
	struct image image;
	enum read_status bmp_r_status = from_bmp(file, &image);
	check_read_status(bmp_r_status);

	// Rotating source.bmp and creating rotated struct image
	struct image rotated_image = rotate_image(image);

	// Saving rotated image to output.bmp
	enum write_status bmp_w_status = to_bmp(file2, &rotated_image);
	check_write_status(bmp_w_status);

	// Close source.bmp and output.bmp
	enum read_status fclose_status1 = file_close(&file);
	check_read_status(fclose_status1);

	enum read_status fclose_status2 = file_close(&file2);
	check_read_status(fclose_status2);

	// Free data
	free(image.data);
	free(rotated_image.data);

	printf("Rotated image saved: %s", argv[2]);
	return 0;
}