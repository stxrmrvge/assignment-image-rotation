#include "file_handler.h"
#include "status.h"

#define READ_MODE "rb"
#define WRITE_MODE "wb"

enum read_status file_read(FILE** file, const char* path)
{
	if (!file) return READ_ERROR;
	if (!path) return READ_PATH_ERROR;
	*file = fopen(path, READ_MODE);
	if (*file == NULL) return READ_PATH_ERROR;
	return READ_OK;
}

enum write_status file_write(FILE** file, const char* path)
{
	if (!*file) return WRITE_ERROR;
	if (!path) return WRITE_PATH_ERROR;
	*file = fopen(path, WRITE_MODE);
	if (*file == NULL) return WRITE_FILE_DOESNT_EXIST;
	return WRITE_OK;
}

enum read_status file_close(FILE** file)
{
	if (!*file) return READ_CLOSE_ERROR;
	int status = fclose(*file);
	return status ? READ_CLOSE_ERROR : READ_OK;
}